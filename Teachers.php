<?php
namespace Teachers;
use \Users\Users as User;
class Teachers extends User
{
    public function getTeacherName(){
        return parent::$firstName ." ".parent::$lastName;
    }
    public function setTeacherName($firstName,$lastName){
        $this->firstName = $firstName;
        $this->lastName = $firstName;
    }
    public function setID()
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $this->ID = "T". substr(str_shuffle($chars), 0, 8);
    }
    protected $income;
    public function setIncome($income)
    {
        $this->income = $income;
    }
    public function getIncome()
    {
        return $this->income;
    }
    public function setStudentScore($studentID , $score){

    }
    protected $classList;
    public function setClassList($className)
    {
        $this->classList[count($this->classList)] = $className;
    }
    public function getClassList()
    {
        return $this->classList;
    }
}