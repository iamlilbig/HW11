<?php
namespace school;
use \Master\Master as Master;
class School
{
    const SCHOOL_NAME = "Alborz";
    const SCHOOL_TYPE = "HighSchool";
    protected $masterName;
    public function setMasterName($masterName)
    {
        $this->masterName = $masterName;
    }
    public function getMasterName()
    {
        return $this->masterName;
    }
    protected $classes;
    public function setClasses($classes)
    {
        $this->classes = $classes;
    }
    public function getClasses()
    {
        return $this->classes;
    }
    protected $students = [];
    public function setStudents($students)
    {
        $this->students[count($this->students)] = $students;
    }
    public function getStudents()
    {
        return $this->students;
    }
    protected $income;
    public function setIncome($income)
    {
        $this->income = $income;
    }
    public function getIncome()
    {
        return $this->income;
    }
    protected $expenses;
    public function setExpenses($expenses)
    {
        $this->expenses = $expenses;
    }
    public function getExpenses()
    {
        return $this->expenses;
    }
}